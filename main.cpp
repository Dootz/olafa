#include <iostream>
#include <cmath>
#include <conio.h>
#include <cstdlib>
#include <stdio.h>


using namespace std;

int s, k;
float *wsk_tablica_wsp;
char wybor, l[11]={'a','b','c','d','e','f','g','h','i','j','k'};
float x; //case 1
float a, b, srodek, dokladnosc=0.001;  //case 2
float *wsk_tablica_przedzialow, h; // case 3

float f(float arg)
{
    float w=0;
    for (int i=0, j=s; i<=s; i++, j--)
    {
        w+=wsk_tablica_wsp[i]*pow(arg,j);
    }
    return w;
}

float calka(int c, int d, int k)
{
    float h, cal, sum;
    h=(float)(d-c)/k;
    wsk_tablica_przedzialow=new float[k+1];

    for(int i=0; i<=k; i++)
    {
        wsk_tablica_przedzialow[i]=c+i*h;
    }

    sum=(f(c)+f(d))/2;

    for(int i=1; i<=k-1; i++)
    {
        sum+=f(wsk_tablica_przedzialow[i]);
    }
    cal=h*sum;

    return cal;
}

void wypisz(float *wsk_tablica_wsp, int s)
{
    for (int i=0, j=s; i<=s; i++, j--)
    {
        if (j==s)
        {
            if (wsk_tablica_wsp[i]!=0&&wsk_tablica_wsp[i]!=1)
            {
                cout << wsk_tablica_wsp[i] << "x^" << j;
            }
            else if (wsk_tablica_wsp[i]==1)
            {
                cout << "x^" << j;
            }
        }
        else if (j<s&&j!=1&&j!=0)
        {
            if (wsk_tablica_wsp[i]!=0&&wsk_tablica_wsp[i]!=1)
            {
                cout << "+" << wsk_tablica_wsp[i] << "x^" << j;
            }
            else if (wsk_tablica_wsp[i]==1)
            {
                cout << "+x^" << j;
            }
        }
        else if (j==1)
        {
            if (wsk_tablica_wsp[i]!=0&&wsk_tablica_wsp[i]!=1)
            {
                cout << "+" << wsk_tablica_wsp[i] << "x";
            }
            else if (wsk_tablica_wsp[i]==1)
            {
                cout << "+" << "x^" << j;
            }
        }
        else if (j==0)
        {
           if (wsk_tablica_wsp[i]!=0)
            {
                cout << "+" << wsk_tablica_wsp[i];
            }
        }
    }
}

int main()
{
    cout << "Podaj stopien wielomianu: ";
    cin >> s;
    cout << endl;

    wsk_tablica_wsp=new float[s+1];

    cout << "Wielomian jest postaci:" << endl;

    for (int i=s, j=0; i>=0; i--, j++)
    {
        if(i!=0&&i!=1)
        cout << l[j] << "x^" << i <<"+";
        else if (i==1)
        cout << l[j] << "x" <<"+";
        else
        cout << l[j];
    }

    cout << endl << endl;

    for (int i=0; i<=s; i++)
    {
        cout << "Podaj wartosc wspolczynnika " << l[i] <<": ";
        cin >> wsk_tablica_wsp[i];
    }

    cout << endl << "Funkcja dana jest rownaniem ";

    wypisz(wsk_tablica_wsp, s);

    cout << endl << endl;
    cout << "Co chcesz wyznaczyc ?" << endl;
    cout << "----------------------------------------------------" << endl;
    cout << "1. Wartosc funkcji dla zadanego argumentu" << endl;
    cout << "2. Miejsce zerowe funkcji w danym przedziale" << endl;
    cout << "3. Wartosc calki oznaczonej w zadanym przedziale" << endl;
    cout << "----------------------------------------------------" << endl;
    cout << endl;

    wybor=_getch();

    switch(wybor)
    {
        case '1':
        {
            cout << "Podaj wartosc argumentu funkcji: ";
            cin >> x;
            cout << "Dla argumentu " << x << " funkcja przyjmuje wartosc " << f(x);
            break;
        }

        case '2':
        {
            cout << "Podaj minimalna wartosc w przedziale:";
            cin >> a;
            cout << "Podaj maksymalna wartosc w przedziale:";
            cin >> b;
            if(f(a)*f(b)>0)
            {
                cout << "Brak miejsca zerowego w zadanym przedziale" << endl;
                break;
            }
            while (abs(a-b)>dokladnosc)
            {
                srodek=(a+b)/2;
                if (f(srodek)==0)
                {
                    cout << "Miejscem zerowym jest " <<srodek;
                    break;
                }
                else if (f(a)*f(srodek)<0)
                {
                    b=srodek;
                }
                else
                {
                    a=srodek;
                }
            }
            cout << "Miejscem zerowym jest " << (a+b)/2 << endl;
        }

        case '3':
        {
            float c,d,k;
            cout << "Podaj minimalna wartosc w przedziale: ";
            cin >> c;
            cout << "Podaj maksymalna wartosc w przedziale: ";
            cin >> d;
            cout << "Podaj krok: ";
            cin >> k;
            cout << "Calka oznaczona z funkcji ";
            wypisz(wsk_tablica_wsp, s);
            cout << " w przedziale <" << c << "," << d << ">" << " wynosi " << calka(c,d,k);

        }
        getchar();getchar();
    }
    return 0;
}
